import { AmplifyAuthenticator } from '@aws-amplify/ui-react'

export default function Home () {
  return (
    <AmplifyAuthenticator>
      Homepage
    </AmplifyAuthenticator>
  )
}
