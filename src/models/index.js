// @ts-check
import { initSchema } from '@aws-amplify/datastore';
import { schema } from './schema';



const { Wallet, Balance } = initSchema(schema);

export {
  Wallet,
  Balance
};