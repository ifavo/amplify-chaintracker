import { ModelInit, MutableModel, PersistentModelConstructor } from "@aws-amplify/datastore";





type WalletMetaData = {
  readOnlyFields: 'createdAt' | 'updatedAt';
}

type BalanceMetaData = {
  readOnlyFields: 'createdAt' | 'updatedAt';
}

export declare class Wallet {
  readonly id: string;
  readonly owner: string;
  readonly name: string;
  readonly network: string;
  readonly address: string;
  readonly balances?: (Balance | null)[];
  readonly isDeleted?: boolean;
  readonly createdAt?: string;
  readonly updatedAt?: string;
  constructor(init: ModelInit<Wallet, WalletMetaData>);
  static copyOf(source: Wallet, mutator: (draft: MutableModel<Wallet, WalletMetaData>) => MutableModel<Wallet, WalletMetaData> | void): Wallet;
}

export declare class Balance {
  readonly id: string;
  readonly walletID: string;
  readonly owner: string;
  readonly token: string;
  readonly balance: number;
  readonly createdAt?: string;
  readonly updatedAt?: string;
  constructor(init: ModelInit<Balance, BalanceMetaData>);
  static copyOf(source: Balance, mutator: (draft: MutableModel<Balance, BalanceMetaData>) => MutableModel<Balance, BalanceMetaData> | void): Balance;
}