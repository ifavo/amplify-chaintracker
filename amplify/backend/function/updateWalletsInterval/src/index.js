/* Amplify Params - DO NOT EDIT
    API_WALLETS_GRAPHQLAPIENDPOINTOUTPUT
    API_WALLETS_GRAPHQLAPIIDOUTPUT
    API_WALLETS_GRAPHQLAPIKEYOUTPUT
    ENV
    REGION
Amplify Params - DO NOT EDIT */

const updateVeChainWallet = require('/opt/nodejs/updateVeChainWallet')
const updateBitcoinWallet = require('/opt/nodejs/updateBitcoinWallet')
const listWallets = require('/opt/nodejs/listWallets')

exports.handler = async () => {
  const wallets = await listWallets()
  for (const wallet of wallets) {
    switch (wallet.network) {
      case 'vechain':
        await updateVeChainWallet(wallet.id)
        break
      case 'bitcoin':
        await updateBitcoinWallet(wallet.id)
        break
    }
  }
}
