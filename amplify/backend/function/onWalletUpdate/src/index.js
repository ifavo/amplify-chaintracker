/* Amplify Params - DO NOT EDIT
  API_WALLETS_GRAPHQLAPIENDPOINTOUTPUT
  API_WALLETS_GRAPHQLAPIIDOUTPUT
  API_WALLETS_GRAPHQLAPIKEYOUTPUT
  ENV
  REGION
Amplify Params - DO NOT EDIT */
const updateVeChainWallet = require('/opt/nodejs/updateVeChainWallet')
const updateBitcoinWallet = require('/opt/nodejs/updateBitcoinWallet')

exports.handler = async (event) => {
  for (const record of event.Records) {
    console.log('DynamoDB Record: %j', record.eventName, record.dynamodb)

    if (record.eventName !== 'INSERT') {
      return
    }

    switch (record.dynamodb.NewImage.network.S) {
      case 'vechain':
        await updateVeChainWallet(record.dynamodb.NewImage.id.S)
        break
      case 'bitcoin':
        await updateBitcoinWallet(record.dynamodb.NewImage.id.S)
        break
    }
  }
}
