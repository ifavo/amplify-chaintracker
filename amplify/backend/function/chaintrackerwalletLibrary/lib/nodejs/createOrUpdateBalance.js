const axios = require('axios')
const gql = require('graphql-tag')
const graphql = require('graphql')
const { print } = graphql

const createBalanceQl = gql`
mutation createBalance($input: CreateBalanceInput!) {
   createBalance(input: $input) {
     balance
     token
   }
 }
`

const updateBalanceQl = gql`
mutation updateBalance($input: UpdateBalanceInput!) {
 updateBalance(input: $input) {
     balance
   }
 }
`

async function addBalance (input) {
  try {
    const { data } = await axios({
      url: process.env.API_WALLETS_GRAPHQLAPIENDPOINTOUTPUT,
      method: 'post',
      headers: {
        'x-api-key': process.env.API_WALLETS_GRAPHQLAPIKEYOUTPUT
      },
      data: {
        query: print(createBalanceQl),
        variables: { input }
      }
    })
    return data.data.createBalance
  } catch (err) {
    console.log('error posting to appsync: ', err)
  }
}

async function updateBalance (input) {
  try {
    const { data } = await axios({
      url: process.env.API_WALLETS_GRAPHQLAPIENDPOINTOUTPUT,
      method: 'post',
      headers: {
        'x-api-key': process.env.API_WALLETS_GRAPHQLAPIKEYOUTPUT
      },
      data: {
        query: print(updateBalanceQl),
        variables: { input }
      }
    })
    return data.data.updateBalance
  } catch (err) {
    console.log('error posting to appsync: ', err)
  }
}

module.exports = async function createOrUpdate ({ token, wallet, balance }) {
  const existingBalance = wallet.balances.items.find(wallet => wallet.token === token)

  if (existingBalance && existingBalance.balance === balance) {
    return
  }

  if (!existingBalance) {
    console.log('Adding Balance to wallet', wallet.address, 'with', balance, token)
    return addBalance({
      walletID: wallet.id,
      owner: wallet.owner,
      balance,
      token
    })
  }

  console.log('Updating Balance in wallet', wallet.address, 'with', balance, token, 'for version', existingBalance._version)
  return updateBalance({
    id: existingBalance.id,
    _version: existingBalance._version,
    balance
  })
}
