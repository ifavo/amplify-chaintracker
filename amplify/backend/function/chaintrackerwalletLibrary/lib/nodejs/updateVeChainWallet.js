const axios = require('axios')
const { utils } = require('ethers')
const createOrUpdateBalance = require('./createOrUpdateBalance')
const getWallet = require('./getWallet')

const EXPLORER_API = 'https://explore-mainnet.veblocks.net/accounts/{address}'

module.exports = async (id) => {
  const wallet = await getWallet(id)
  if (!wallet) {
    console.log('Wallet with id', id, 'not found')
    return
  }

  console.log('Updating wallet', wallet.address, 'for', wallet.owner)
  const { data } = await axios.get(EXPLORER_API.replace('{address}', wallet.address))
  console.log('Balance for wallet', wallet.address, 'is', data.balance, data.energy)

  await createOrUpdateBalance({ wallet, balance: utils.formatEther(data.balance), token: 'vet' })
  await createOrUpdateBalance({ wallet, balance: utils.formatEther(data.energy), token: 'vtho' })
}
