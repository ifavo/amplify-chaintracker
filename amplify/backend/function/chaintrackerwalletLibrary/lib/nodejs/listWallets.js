const axios = require('axios')
const gql = require('graphql-tag')
const graphql = require('graphql')
const { print } = graphql

const listAllWallets = gql`
query listWallets {
  listWallets(filter: {isDeleted: {eq: false}}) {
    items {
      id
      network
      address
    }
  }
}
`

module.exports = async function listWallets () {
  try {
    const graphqlData = await axios({
      url: process.env.API_WALLETS_GRAPHQLAPIENDPOINTOUTPUT,
      method: 'post',
      headers: {
        'x-api-key': process.env.API_WALLETS_GRAPHQLAPIKEYOUTPUT
      },
      data: {
        query: print(listAllWallets)
      }
    })
    return graphqlData.data.data.listWallets.items
  } catch (err) {
    console.log('error posting to appsync: ', err)
  }
}
