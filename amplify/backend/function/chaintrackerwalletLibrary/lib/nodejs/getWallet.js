const axios = require('axios')
const gql = require('graphql-tag')
const graphql = require('graphql')
const { print } = graphql

const getWalletQl = gql`
query GetWallet($id: ID!) {
  getWallet(id: $id) {
    id
    owner
    name
    network
    address
    balances {
      items {
        id
        walletID
        owner
        token
        balance
        createdAt
        updatedAt
        _version
      }
      nextToken
    }
    isDeleted
    createdAt
    updatedAt
  }
}
`

module.exports = async function getWallet (id) {
  try {
    const graphqlData = await axios({
      url: process.env.API_WALLETS_GRAPHQLAPIENDPOINTOUTPUT,
      method: 'post',
      headers: {
        'x-api-key': process.env.API_WALLETS_GRAPHQLAPIKEYOUTPUT
      },
      data: {
        query: print(getWalletQl),
        variables: { id }
      }
    })
    return graphqlData.data.data.getWallet
  } catch (err) {
    console.log('error posting to appsync: ', err)
  }
}
