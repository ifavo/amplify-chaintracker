const axios = require('axios')
const bitcoin = require('bitcoin-units')

const createOrUpdateBalance = require('./createOrUpdateBalance')
const getWallet = require('./getWallet')

const EXPLORER_API = 'https://api.blockcypher.com/v1/btc/main/addrs/{address}/balance'
const TOKEN = 'btc'

module.exports = async (id) => {
  const wallet = await getWallet(id)
  if (!wallet) {
    console.log('Wallet with id', id, 'not found')
    return
  }

  console.log('Updating wallet', wallet.address, 'for', wallet.owner)
  const { data } = await axios.get(EXPLORER_API.replace('{address}', wallet.address))
  const balance = bitcoin(data.final_balance, 'satoshi').to('BTC').value()
  console.log('Balance for wallet', wallet.address, 'is', balance)

  await createOrUpdateBalance({ wallet, balance, token: TOKEN })
}
